<?php
/**
 * User: 假如_丶 <897645119@qq.com>
 * Date: 2020/5/9
 * Time: 14:10
 * 无可奈何花落去，似曾相识燕归来。
 */


namespace fanxd\repository\command\transform;

abstract class Transform
{
    /**
     * @param $items
     * @return array
     */
    public function transformPages($items)
    {
        $items = [
            'total'         => $items['total'],
            'perPage'       => $items['per_page'],
            'currentPage'   => $items['current_page'],
            'lastPage'      => $items['last_page'],
            'data'          => array_map([$this, 'transform'], $items['data'])
        ];

        return $items;
    }

    /**
     * @param $items
     * @return array
     */
    public function transformCollection($items)
    {
        return array_map([$this, 'transform'], $items);
    }

    /**
     * @param $items
     * @return mixed
     */
    public abstract function transform($items);
}